package com.example.myapplication;

import android.os.Build;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.example.myapplication.adpater.UserListAdpater;
import com.example.myapplication.util.Const;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

public class SecondActivity extends AppCompatActivity {


    ListView lvUsers;
    ArrayList<HashMap<String, Object>> userList = new ArrayList<>();
    UserListAdpater userListAdpater;
    //Spinner spUsers;
    //  LinearLayout llMain;


    @RequiresApi(api = Build.VERSION_CODES.M)
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);
        initViewRefernce();
        bindViewValues();
    }
    // Intent intent = getIntent();
    //String st = getIntent().getExtras().getString("value");

    //TextView textView = (TextView) findViewById(R.id.text);
    //textView.setText(st);

    //Toast.makeText(getApplicationContext(), "successful login", Toast.LENGTH_SHORT).show();

    @RequiresApi(api = Build.VERSION_CODES.M)
    void bindViewValues() {
        userList.addAll((Collection<? extends HashMap<String, Object>>) getIntent().getSerializableExtra("UserList"));
        /*for(int i=0;i<userList.size();i++)
        {
            View view= LayoutInflater.from(SecondActivity.this).inflate(R.layout.view_row_user_list,null);
            TextView tvName = view.findViewById(R.id.tvLstName);
            TextView tvEmail = view.findViewById(R.id.tvLstEmail);
            TextView tvGender = view.findViewById(R.id.tvLstGender);

            tvName.setText(userList.get(i).get(Const.FIRST_NAME) + "" + userList.get(i).get(Const.LAST_NAME));
            tvGender.setText(userList.get(i).get(Const.GENDER).toString());
            tvEmail.setText(String.valueOf(userList.get(i).get(Const.EMAIL_ADDRESS)));
            llMain.addView(view);
        }*/
       /* userListAdpater = new UserListAdpater(this, userList);
        lvusers.setAdapter(userListAdpater);
        lvusers.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long itemId) {
                Toast.makeText(SecondActivity.this, userList.get(position).get(Const.GENDER).toString(), Toast.LENGTH_SHORT).show();
            }
        });*/

        userListAdpater = new UserListAdpater(this, userList);
        lvUsers.setAdapter(userListAdpater);
        //spUsers.setAdapter(userListAdpater);

        lvUsers.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                Toast.makeText(SecondActivity.this, userList.get(position).get(Const.GENDER).toString(), Toast.LENGTH_SHORT).show();
            }
        });

        /*spUsers.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                Toast.makeText(SecondActivity.this, userList.get(position).get(Const.GENDER).toString(), Toast.LENGTH_SHORT).show();
            }
        });*/

    }

    void initViewRefernce() {
        lvUsers = findViewById(R.id.lvActuserList);
        //spUsers = findViewById(R.id.spActuser);
        getSupportActionBar().setTitle(R.string.lbl_display);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.dashboard_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }
}

